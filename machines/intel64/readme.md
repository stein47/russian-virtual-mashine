# :ru:  Проект "Виртуальная машина"

Реализация виртуальной машины в рамках проекта https://plana.mybb.ru/viewforum.php?id=96
<a href="http://plana.mybb.ru/viewtopic.php?id=1916">Среда разработки</a> для русифицированного С/С++ на базе редактора Kate


## <a name=Оглавление>Оглавление</a>

1. <a href="#Новости-проекта">Новости проекта</a>
2. <a href="#Сборка-проекта">Сборка проекта</a>
3. <a href="#Состав-проекта">Состав проекта</a>
4. <a href="https://tvoygit.ru/stein47/russian-virtual-mashine.wiki.git">Вики</a>

### <a name=Новости-проекта>Новости проекта :new:</a>

:ru: **Язык исходников - русифицированный С**
В настоящий момент проект находится в стартовом состоянии.
```C #вставка    "keywords.h"
#вставка    "ru_stdio.h"
#вставка 	<string.h>
#вставка	"ansicode.h"
#вставка	"opcode.h"
#вставка	"vm.h"		//Внешний интерфейс виртуальной машины
// Задачи проекта
цел main()
{
 	пчф(ЗЕЛЕНЫЙ_ТЕКСТ "Новый проект.\n" ОБЫЧНЫЙ_ТЕКСТ);
	//в переменной типа ТВМ "упакована" вся внутреннее устройство ВМ:
	// регистры, стек, память указатели и тд.
	ТВМ* ВМ1 = создать_ВМ();

	// программа состоящая их четырех тестовых инструкций.
	б64 программа[4] = {цСЛОЖ, цВЫЧ, цУМН, СТОП} ;
	//Программа загружается как массив байтов.
	загрузить_ВМ(ВМ1,(симв*) программа, (симв*)((программа)+1));

	старт_ВМ(ВМ1);
	// освобождаем динамически выделенную память под ВМ.
	закрыть_ВМ (ВМ1);
	пчф("Виртуальная машина успешно завершила работу.\n Для завершения нажмите ВВОД");
	чтз();
	вернуть 0;
}
```
<a href="#Оглавление">:arrow_up:Оглавление</a>
### <a name=Сборка-проекта>Сборка проекта 🛠️</a>
- Для сборки проекта используйте команду: ***make all***
- Для удаления временных файлов (очистки проекта) использовать команду: ***make clean***
- Запустить собранную программу: ***make run***
- Сделать резервную копию проекта: ***make reserve***
- Пересобрать и запустить: ***make clean all run***

Проект уже сейчас содержит большое количество файлов. Для автоматизации процесса сборки используется утилита make.
Основной управляющий файл для сборки Makefile, лежит в корневой папке. Два вспомогательных лежат в папках src_c\, src_rus\. Вспомогательные нужны для запуска сборки из этих папок,а не из корневой.
Для запуска процесса сборки необходимо в консоли перейти в корневую папку проекта (или в одну из папок src_c\, src_rus)после чего запустить процесс

<a href="#Оглавление">:arrow_up:Оглавление</a>

### <a name=Состав-проекта>Состав проекта :page_facing_up:</a>

<a name=begin-sostav></a>
- :open_file_folder:src_rus - папка с исходниками на русифицированном С
	- ansicode.rh
	- debug.rh
	- interface.rh
	- main.r
	- opcode.rh
	- vm_core.rh
	- vm.r
	- vm.rh
- :open_file_folder:src_c - папка с исходниками на стандартном С
- :open_file_folder:include - папка с исходниками на стандартном С для русификации
	- keywords.h
	- ru_stdio.h
<a name=end-sostav></a>

<a href="#Оглавление">:arrow_up:Оглавление</a>
