#ifndef     KEYWORDS_H
#define     KEYWORDS_H
// Макросы замены ключевых слов
#define     тип         typedef

#define     симв        char
#define     короткий    short
#define     длин        long
#define     беззнак     unsigned
#define     знак     	signed
#define     цел         int
#define     пуст        void
#define     конст       const
#define    	двойн 		double
#define    	плав 		float

#define     структура   struct

#define     для         for
#define     если        if
#define     иначе       else
#define     пока        while
#define     цикл        do
#define     вернуть     return
#define     выбор     	switch
#define     вариант     case
#define     прервать    break
#define     заново     	continue
#define     прочее     	default
#define    	переход 	goto

#define     перечисление    enum
#define     объединение union



// Ключевые слова стандарта C89
#define    	авто 		auto
#define     внеш 		extern

#define 	регистр 	register

#define    	байторазмер sizeof
#define    	статич 		static
#define 	ненадеж 	volatile

//Ключевые слова стандарта C99
//_Bool       	-нет-
//_Complex	-нет-
//_Imaginary	-нет-
#define  	подстав 	inline
#define 	огранич 	restrict

//Предопределенные макросы
#define 	__ФАЙЛ__ 	__FILE__
#define 	__ДАТА__ 	__DATE__
#define 	__СТРОКА__ 	__LINE__
#define 	__СТДС__ 	__STDC__
#define 	__ВРЕМЯ__ 	__TIME__

#endif    // KEYWORDS_H
