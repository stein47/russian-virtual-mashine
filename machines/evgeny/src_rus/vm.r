//===========================================================
// 	Файл:		vm.r
// 	Версия:		1.00
// 	Дата:		17.06.2022
//	Описание:	Реализация интерфейса виртуальной машины (ВМ)
//				доступ к внутренним структурам ВМ извне напрямую запрещен
//	Автор:
//===========================================================
#вставка    <stdlib.h>

#вставка    "keywords.h"
#вставка    "ru_stdio.h"
#вставка	"vm.h"
#вставка	"vm_core.h"
#вставка	"opcode.h"

//включили отладочный режим
#макрос ОТЛАДКА
#вставка	"debug.h"

/*
 тип  объединение Tкоманда{
    struct {
        б8 опкод;
        б8 режим;
        б8 пустой;
        беззнак рег_приемник: 4;
        беззнак рег_источник: 4;
		б32 адрес;

	}разбить;
	б64 команда;
}Tкоманда64;
 */
#макрос		КОМАНДА
#макрос		ОПКОД 			структура_команды.опкод
#макрос 	РЕЖИМ			структура_команды.режим
#макрос		РЕГ_ПРИЕМНИК	структура_команды.рег_приемник
#макрос 	РЕГ_ИСТОЧНИК	структура_команды.рег_источник
#макрос		АДРЕС			структура_команды.адрес
//имя ВМ, используемое в
#макрос ВМ	эта_ВМ
//элементы ВМ
#макрос РЕГИСТРЫ		ВМ->регистры
#макрос	ПАМЯТЬ 			ВМ->память
#макрос РЕГИСТР_КОМАНД	ВМ->регистр_команд


#макрос 	МАСКА_РЕЖИМА  0b11111

#если (МАСКА_РЕЖИМА == 0b0001)

#макрос ОБРАБОТАТЬ(_команда) \
РЕГИСТРЫ[_команда.РЕГ_ПРИЕМНИК] ОПЕРАЦИЯ РЕГИСТРЫ[_команда.РЕГ_ИСТОЧНИК];


#иначе_если (МАСКА_РЕЖИМА == 0b11111)

#макрос ОБРАБОТАТЬ(_команда, _операция, _строка) 							\
если (_команда.РЕЖИМ == РЕГИСТР_РЕГИСТР){									\
РЕГИСТРЫ[_команда.РЕГ_ПРИЕМНИК] _операция РЕГИСТРЫ[_команда.РЕГ_ИСТОЧНИК];	\
пчс_отл(_строка "регистр-регистр");					\
прервать;																	\
}																			\
если (_команда.РЕЖИМ == РЕГИСТР_ПАМЯТЬ){									\
РЕГИСТРЫ[_команда.РЕГ_ПРИЕМНИК] _операция ПАМЯТЬ[_команда.АДРЕС];			\
пчс_отл(_строка "регистр-память");					\
прервать;																	\
}																			\
если (_команда.РЕЖИМ == ПАМЯТЬ_РЕГИСТР){									\
ПАМЯТЬ[_команда.АДРЕС] _операция РЕГИСТРЫ[_команда.РЕГ_ПРИЕМНИК] ;			\
пчс_отл(_строка "память-регистр");					\
прервать;																	\
}																			\
если (_команда.РЕЖИМ == РЕГИСТР_ДАННЫЕ){									\
РЕГИСТРЫ[_команда.РЕГ_ПРИЕМНИК] _операция ( (*(РЕГИСТР_КОМАНД+1)).данные);	\
РЕГИСТР_КОМАНД++;															\
пчс_отл(_строка "регистр-данные");					\
прервать;																	\
}																			\
если (_команда.РЕЖИМ == ПАМЯТЬ_ДАННЫЕ){										\
ПАМЯТЬ[_команда.АДРЕС] _операция ((*(РЕГИСТР_КОМАНД+1)).данные);				\
РЕГИСТР_КОМАНД++;															\
пчс_отл(_строка "память-данные");					\
прервать;																	\
}																			\
пчс_ош(_строка "- ошибка в структуре: "); 									\
пчф("\tневерная команда: %#llx\n", _команда.данные);						\
переход ВЫХОД_ПО_ОШИБКЕ;

#конец_если

// Арифметические команды для режимов: 	регистр-регистр
//										регистр-память
//										память-Регистр
//										регистр-данные
//										память данные
#макрос ОБРАБОТАТЬ_СЛОЖ(_команда) ОБРАБОТАТЬ(_команда, +=, "команда целочисленного сложения ")
#макрос ОБРАБОТАТЬ_ВЫЧ(_команда) ОБРАБОТАТЬ(_команда, -=, "команда целочисленного вычитания ")
#макрос ОБРАБОТАТЬ_УМН(_команда) ОБРАБОТАТЬ(_команда, *=, "команда целочисленного умножения ")


// Вспомогательная
статич пуст печатать_состояние_ВМ (ТВМ* эта_ВМ)
{
	пчф(" Состояние регистров \n===================================================\n");
	для(цел индекс =0; индекс < 16; индекс++)
	{
		пчф(" Регистр %2i   %#llx\n", индекс, эта_ВМ->регистры[индекс]);
	}
	пчф("===================================================\n");

}
// Конструктор виртуальной машины
ТВМ* создать_ВМ()
{
	ТВМ* врем = (ТВМ*) malloc (sizeof(ТВМ));
	/****начало тестового блока****/
	врем->имя[0] = 'V';
	врем->имя[1] = 'M';
	врем->имя[2] = '1';
	врем->имя[3] = '\n';
	врем->имя[4] = '\0';
	/****конец тестового блока****/
	вернуть врем;
}
// Деструктор виртуальной машины
пуст закрыть_ВМ(ТВМ* эта_ВМ)
{
	free(эта_ВМ);
}
// загрузка программы в байт-кодах в ВМ
пуст загрузить_ВМ(ТВМ* эта_ВМ, симв * старт_адрес, симв* стоп_адрес)
{
	эта_ВМ -> регистр_команд = ((Tкоманда64*) старт_адрес);
}	

//Запуск виртуальной машины
пуст старт_ВМ(ТВМ* эта_ВМ)
{

	цел выполнено_команд = 0;
	для(; (*эта_ВМ -> регистр_команд).структура_команды.опкод!= СТОП; эта_ВМ ->регистр_команд++){
		выбор ((*эта_ВМ -> регистр_команд).структура_команды.опкод ){
			вариант цСЛОЖ:
			//Первая команда сложения
				ОБРАБОТАТЬ_СЛОЖ((*эта_ВМ -> регистр_команд) );
			прервать;
			вариант цВЫЧ:
				ОБРАБОТАТЬ_ВЫЧ((*эта_ВМ -> регистр_команд) );
			прервать;
			вариант цУМН:
				ОБРАБОТАТЬ_УМН((*эта_ВМ -> регистр_команд) );
			прервать;

			прочее:
				пчф_ош("нет такой команды %u\n", (*эта_ВМ -> регистр_команд).структура_команды.опкод);
				переход ВЫХОД_ПО_ОШИБКЕ;
		}
		выполнено_команд++;
	}
	//ТЕСТОВОЕ СООБЩЕНИЕ
	выбор (выполнено_команд > 15 ? выполнено_команд % 10 : выполнено_команд)
	{
		вариант 1:
			пчф("выполнена %i команда\n", выполнено_команд);
		прервать;
		вариант 2:
		вариант 3:
		вариант 4:
			пчф("выполнено %i команды\n", выполнено_команд);
		прервать;
		прочее:
			пчф("выполнено %i команд\n", выполнено_команд);
	}
	если (0){
		ВЫХОД_ПО_ОШИБКЕ:
		//Здесь находится блок обработки ошибки
			пчф_ош("Виртуальная машина завершила работу с ошибкой\n");
			печатать_состояние_ВМ(эта_ВМ);
			вернуть;
	}
	пчф("Виртуальная машина успешно завершила работу.\n");

}
