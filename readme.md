# :ru:  Проект "Виртуальная машина"

Реализация виртуальной машины в рамках проекта https://plana.mybb.ru/viewforum.php?id=96   
<a href="http://plana.mybb.ru/viewtopic.php?id=1916">Среда разработки</a> для русифицированного С/С++ на базе редактора Kate


## <a name=Оглавление>Оглавление</a>

1. <a href="#Новости-проекта">Новости проекта</a>
2. <a href="#Сборка-проекта">Сборка проекта</a>
3. <a href="#Состав-проекта">Состав проекта</a>
4. <a href="https://tvoygit.ru/stein47/russian-virtual-mashine.wiki.git">Вики</a>

### <a name=Новости-проекта>Новости проекта :new:</a>

:ru: **Язык исходников - русифицированный С**   
Проект преобразован в мультипроект. Появилась папка ***machines***, в которой лежат самостоятельные
проекты виртуальных машин.   
Подробнее смотри <a href="http://plana.mybb.ru/viewtopic.php?id=1966#p8912">ветку на форуме</a>

<a href="#Оглавление">:arrow_up:Оглавление</a>
### <a name=Сборка-проекта>Сборка проекта 🛠️</a>
- Для настройки конфигурации используйте: ***autoreconf -iv***
- Создание Makefile:  ***./configure***
- Для сборки проекта используйте команду: ***make all***
- Все три предыдущие команды в одном флаконе: ***autoreconf -ivm*** 
- Для удаления временных файлов (очистки проекта) использовать команду: ***make clean***
- Пересобрать: ***make clean all***
- Запустить собранную программу: ***sk -m intel64 file_bytecode.byte***
- Чтобы собрать deb-пакет без установки используйте: ***sudo checkinstall install=no --nodoc --pkglicense=MIT -D***
- Просто установить без создания пакета: ***sudo make install***
- Удалить установленную программу: ***sudo make uninstall***

<a href="#Оглавление">:arrow_up:Оглавление</a>

### <a name=Состав-проекта>Состав проекта :page_facing_up:</a>

<a name=begin-sostav></a>
- :open_file_folder:src_rus - папка с исходниками на русифицированном С
	- ansicode.rh
	- debug.rh
	- main.r
- :open_file_folder:src_c - папка с исходниками на стандартном С
- :open_file_folder:include - папка с исходниками на стандартном С для русификации
	- keywords.h
	- ru_stdio.h
- :open_file_folder:machines - папка с проектами отдельных виртуальных машин
	- :open_file_folder:evgeny - папка проекта ВМ
	- :open_file_folder:intel64 - папка проекта ВМ
<a name=end-sostav></a>

<a href="#Оглавление">:arrow_up:Оглавление</a>
